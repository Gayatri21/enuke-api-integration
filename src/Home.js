import React, { useState, useEffect } from "react";

const Home = () => {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [dataTime, setDateTime] = useState([]);

  useEffect(() => {
    fetch(
      "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=IBM&interval=5min&apikey=demo"
    )
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result["Time Series (5min)"]);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  useEffect(() => {
    const storeData = Object.keys(items).map((list) => {
      return list;
    });
    setDateTime(storeData);
  }, [items]);

  return (
    <div>
    {isLoaded ?
    <table class="center">
        <tr>
          <th>DateTime</th>
          <th>Open</th>
          <th>High</th>
          <th>Low</th>
          <th>Close</th>
          <th>Volume</th>
        </tr>
        {dataTime?.map((list) => (
          <tr>
            <td>{list}</td>
            <td>{items[list]["1. open"]}</td>
            <td>{items[list]["2. high"]}</td>
            <td>{items[list]["3. low"]}</td>
            <td>{items[list]["4. close"]}</td>
            <td>{items[list]["5. volume"]}</td>
          </tr>
        ))}
      </table>
      
       :'Loading' }
      
    </div>
  );
};

export default Home;
